#!/bin/bash


DELLY_VCF=$1
GENOME_FA=$2


if [[ $# -lt 2 ]]
then
echo -e "syntax: <delly.vcf> <genome_fastaFile> "
exit -1
fi
DATE=$(date +"%Y%m%d")

DELLY_VERSION="2-0-0"

DELLY_RN_RAW_FILE="${DELLY_VCF}.tmp1"
DELLY_RN_FILE="${DELLY_VCF}.tmp2"
DELLY_PCAWG_FILE_PRE="${DELLY_VCF}.tmp3"
NAME_BASE=$(dirname $DELLY_VCF)/$(basename $DELLY_VCF | awk -F'[.]' '{print $1}')
DELLY_PCAWG_FILE="${NAME_BASE}.bric_embl-delly_${DELLY_VERSION}.${DATE}.somatic.sv.vcf.gz"

delly_scna_vcf_flag.py -v ${DELLY_VCF}  -o ${DELLY_RN_RAW_FILE} -c ${DELLY_RN_FILE}
ls  -l ${DELLY_RN_FILE}
echo -e "\n>> reformat to two lines per SV <<\n"
## reformat to two lines per SV and add nt at breakpoint ##
dellyVcf2BndVcf.py -v ${DELLY_RN_FILE} -o ${DELLY_PCAWG_FILE_PRE} -g ${GENOME_FA}

echo -e "\n>> remove tumor frq annotation and validate vcf<<\n"
### remove tumor frq annotation ##
cat ${DELLY_PCAWG_FILE_PRE} | grep -v '##INFO=<ID=SC_' | sed 's/;SC_[^;]*//g' | vcf-sort | bgzip -c  > ${DELLY_PCAWG_FILE}
vcf-validator ${DELLY_PCAWG_FILE}

echo -e "\n>> Make BEDPE <<\n"
## make BEDPE files ##
dellyVcf2Tsv.py -v ${DELLY_RN_FILE} -o ${DELLY_PCAWG_FILE/.vcf*/.bedpe.txt}
cp -r ${DELLY_PCAWG_FILE/.vcf*/.bedpe.txt} ${NAME_BASE}.somatic.highconf.plot.bedpe

echo -e "\n>> package up<< \n"
## package up
md5sum ${DELLY_PCAWG_FILE} | awk '{print $1}' > ${DELLY_PCAWG_FILE}.md5

tabix -p vcf ${DELLY_PCAWG_FILE}

md5sum ${DELLY_PCAWG_FILE}.tbi | awk '{print $1}' > ${DELLY_PCAWG_FILE}.tbi.md5

echo -e "\n\t## done with ${DELLY_PCAWG_FILE} ##\n"

rm ${DELLY_VCF}.tmp*

#! /usr/bin/env python2.7
from __future__ import division
import pandas as pd
import sys
import gzip
import argparse
from pysam import VariantFile


def xopen(filename, mode='r'):
    '''
    Replacement for the "open" function that can also open
    files that have been compressed with gzip. If the filename ends with .gz,
    the file is opened with gzip.open(). If it doesn't, the regular open()
    is used. If the filename is '-', standard output (mode 'w') or input
    (mode 'r') is returned.
    '''
    assert isinstance(filename, str)
    if filename == '-':
        return sys.stdin if 'r' in mode else sys.stdout
    if filename.endswith('.gz'):
        return gzip.open(filename, mode)
    else:
        return open(filename, mode)


def bin_filter(df):
    bins = [0, 1, 2000, 5000, 10000, df['size'].max()]
    df['bin_point'] = pd.cut(df['size'], bins, include_lowest=True)
    count = pd.value_counts(df['bin_point'])
    percentage = 100 * count / len(df.index)
    prc = percentage[percentage > 29]
    for index_val, series_val in prc.iteritems():
        if '[0, 1]' in index_val:
            filtered_list1 = mapq_filter(df)
            return filtered_list1
        if '(2000, 5000]' in index_val:
            tmp_df = df.ix[(df['size'] > 1) & (df['size'] < 5000)]
            df.set_index('id', inplace=True)
            tmp_df.set_index('id', inplace=True)
            diff_filtered_df = df[~df.index.isin(tmp_df.index)]
            filtered_df = diff_filtered_df.reset_index()
            filtered_list2 = filtered_df['id'].tolist()
            return filtered_list2
        else:
            df.set_index('bin_point', inplace=True)
            diff_filtered_df = df[~df.index.isin(prc.index)]
            filtered_df = diff_filtered_df.reset_index()
            filtered_list3 = filtered_df['id'].tolist()
            return filtered_list3
        merged_idlist = list(set(filtered_list1 + filtered_list2 +
                                 filtered_list3))
        return merged_idlist


def mapq_filter(df):
    BND = df.ix[(df['size'] < 1)]
    tmp_df = BND.ix[(BND['pairs'] <= 10) & (BND['mapq'] < 60)]
    percentage = 100 * len(tmp_df) / len(BND.index)
    if percentage > 90:
        df.set_index('id', inplace=True)
        tmp_df.set_index('id', inplace=True)
        diff_filtered_df = df[~df.index.isin(tmp_df.index)]
        filtered_df = diff_filtered_df.reset_index()
        return filtered_df['id'].tolist()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--vcf',  help='VCF input', required=True)
    parser.add_argument('-o', '--out',  help='VCF output', default='-')
    parser.add_argument('-b', '--bedpe',
                        help='Bedpe converted input vcf', required=True)
    parser.add_argument('-s', '--size',
                        help='Max size', type=int, default=1000000)
    args = parser.parse_args()

    with xopen(args.bedpe, 'rt') as bedpe:
        df = pd.read_table(bedpe, sep='\t', low_memory=False)
        if len(df.index) > args.size:
            merged_idlist = bin_filter(df)
            if len(merged_idlist) == 0:
                print "Sv noise filter not triggerd"
            else:
                if args.out.endswith('.bcf'):
                    write = 'wb'
                else:
                    write = 'w'
                with VariantFile(args.vcf) as vcf_in:
                    with VariantFile(args.out, write, header=vcf_in.header) \
                            as vcf_out:
                        for rec in vcf_in.fetch():
                            if rec.id in merged_idlist:
                                vcf_out.write(rec)


if __name__ == "__main__":
    main()

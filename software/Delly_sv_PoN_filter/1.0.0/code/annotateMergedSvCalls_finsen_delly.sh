#!/bin/bash
aliquot_id=$1
RAW_VCF=$2
DIR=$3
SVCALLCOLLECTION=$4
GENOME_FA=$5

if [[ $# -lt 5 ]]
then
echo -e "syntax: <aliquot_id> <delly_raw.vcf.gz> <output_directory> <SvcallCollection> <Genome_fasta_file>"
exit -1
fi

SAMPLE_NAME=${aliquot_id}
echo $SAMPLE_NAME
DIR_PROJECT=${DIR}/filter_calls
mkdir -p $DIR_PROJECT
chmod -R 0777 $DIR_PROJECT
echo ${DIR_PROJECT}
echo $RAW_VCF

CUTOFF_SV_OVERHANG=1000

svSuffix=sv.vcf.gz

SV_FILE_BASE=$DIR_PROJECT/$SAMPLE_NAME

SV_FILE=${RAW_VCF}

echo -e "raw vcf\n" `ls -lh ${SV_FILE}`

cp ${SV_FILE} ${SV_FILE_BASE}.tmp.vcf.gz
tabix -f -p vcf ${SV_FILE_BASE}.tmp.vcf.gz
tabix -f -p vcf ${SV_FILE}

vcf2tsv -n "." ${SV_FILE} | awk 'BEGIN{ OFS="\t" }{ if (NR==1) { for (i=1; i<=NF; i++) { c[$i]=i };} print $c["CHROM"],$c["POS"]-'$CUTOFF_SV_OVERHANG',$c["POS"]+'$CUTOFF_SV_OVERHANG',$c["CHR2"],$c["END"]-'$CUTOFF_SV_OVERHANG',$c["END"]+'$CUTOFF_SV_OVERHANG',$c["ID"],
$c["MAPQ"], $c["CT"],$c["ID"],NR,".",".",".",$c["ID"],$c["CHROM"]"%",$c["POS"] }' | sed 's/\.fa\t/\t/g;s/chr//;s/chr//;s/3to5/+\t-/;s/5to3/-\t+/;s/3to3/+\t+/;s/5to5/-\t-/;s/NtoN/.\t./' | sed '1d'  > ${SV_FILE_BASE}.tmp.bedpe

echo $SVCALLCOLLECTION
for j in $SVCALLCOLLECTION/1KGP $SVCALLCOLLECTION/*/{tumor,control}; do
    if [[ $(basename $j) == "1KGP" ]]; then
        COMPARISON_SAMPLE_SET="1KGP"
        count=1
        for i in ${j}/combinedSVs.bedpe.part*; do
            echo $i
            bedtools pairtopair  -is -a ${SV_FILE_BASE}.tmp.bedpe -b $i > ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.p${count}
            count=$(( $count + 1 ))
        done
    else
        COMPARISON_SAMPLE_SET="$(basename $(dirname $j))_$(basename $j)"
        echo $j
        count=1
        for i in ${j}/combinedSVs.bedpe.part*; do
            echo $i
            bedtools pairtopair  -is -a ${SV_FILE_BASE}.tmp.bedpe -b $i > ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.p${count}
            count=$(( $count + 1 ))
        done
    fi
    if [[ -f ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.p1 ]]; then
        cat ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.p* | cut -f1-18,25 | sort | uniq | cut -f1-18 | cat -  ${SV_FILE_BASE}.tmp.bedpe | sort -k7n | uniq -c | awk 'BEGIN{ OFS="\t" }{ $1=$1-1; print }' > ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.sampleCount
    fi
    cat $j/numberOfSamples.txt > ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.totalNumSamples
    cat  ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.totalNumSamples

    zgrep "^#" ${SV_FILE} | sed 's/FILTER\tINFO\tFORMAT.*/FILTER\tINFO/' | sed 's/\(##INFO=<ID=CIEND.*\)/##INFO=<ID=SC_'$COMPARISON_SAMPLE_SET'_C,Number=1,Type=Float,Description="Count of variant in '$COMPARISON_SAMPLE_SET'">\n##INFO=<ID=SC_'$COMPARISON_SAMPLE_SET'_F,Number=1,Type=Float,Description="Frequency of variant in '$COMPARISON_SAMPLE_SET'">\n\1/' > ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.sampleCount.vcf
    cat ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.sampleCount | awk -v sampleCount=`cat ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.totalNumSamples` 'BEGIN{ OFS="\t" }{ print $18,$19,$17,".",".",".",".","SC_'$COMPARISON_SAMPLE_SET'_C="$1";SC_'$COMPARISON_SAMPLE_SET'_F="int(($1/sampleCount)*10000)/10000 }' | sed 's/%//' >> ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.sampleCount.vcf

    vcfaddinfo ${SV_FILE_BASE}.tmp.vcf.gz <(vcf-sort -c ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.sampleCount.vcf)  > ${SV_FILE_BASE}.svFreq.vcf

    cp ${SV_FILE_BASE}.svFreq.vcf ${SV_FILE_BASE}.tmp.vcf
    bgzip -f ${SV_FILE_BASE}.tmp.vcf
    tabix -f -p vcf ${SV_FILE_BASE}.tmp.vcf.gz
    rm ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}*
done

rm ${SV_FILE_BASE}.tmp.*
date

if [[ -f ${SV_FILE_BASE}.svFreq.vcf ]]; then
    DellySomaticFreqFilter.py -v ${SV_FILE_BASE}.svFreq.vcf -p
    for vcf in ${SV_FILE_BASE}.svFreq.*vcf; do
        echo $vcf
        dellyVcf2Tsv.py -v ${vcf} -o ${vcf/.vcf*/.bedpe.txt}
        bgzip -f $vcf
        tabix -f -p vcf ${vcf}.gz
    done
fi
echo "Apply Noise filter"

max_size=10000
value=$(wc -l <"${SV_FILE_BASE}.svFreq.PoN.somatic.highConf.bedpe.txt")
echo $value
echo $max_size
if [ $value -gt $max_size ]
then
  sv_noise_filter.py -v ${SV_FILE_BASE}.svFreq.PoN.somatic.highConf.vcf.gz -o ${SV_FILE_BASE}.svFreq.PoN.noiseFiltered.somatic.highConf.vcf.gz -b ${SV_FILE_BASE}.svFreq.PoN.somatic.highConf.bedpe.txt -s 10000
  dellyVcf2Tsv.py -v ${SV_FILE_BASE}.svFreq.PoN.noiseFiltered.somatic.highConf.vcf.gz -o ${SV_FILE_BASE}.svFreq.PoN.noiseFiltered.somatic.highConf.bedpe.txt
  echo "filter and reformat to BND format"
  bash dellyVcf2BndVcf.sh ${SV_FILE_BASE}.svFreq.PoN.noiseFilterd.somatic.highConf.vcf.gz ${GENOME_FA}
else
  echo "filter and reformat to BND format"
  bash dellyVcf2BndVcf.sh ${SV_FILE_BASE}.svFreq.PoN.somatic.highConf.vcf.gz ${GENOME_FA}
fi

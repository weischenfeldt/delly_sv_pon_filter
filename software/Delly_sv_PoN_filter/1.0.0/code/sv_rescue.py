#! /usr/bin/env python2.7
from __future__ import division

import argparse
import gzip
import sys
import pybedtools
from pysam import VariantFile

###
# Misc Utilities
###


def xopen(filename, mode='r'):
    '''
    Replacement for the "open" function that can also open
    files that have been compressed with gzip. If the filename ends with .gz,
    the file is opened with gzip.open(). If it doesn't, the regular open()
    is used. If the filename is '-', standard output (mode 'w') or input
    (mode 'r') is returned.
    '''
    assert isinstance(filename, str)
    if filename == '-':
        return sys.stdin if 'r' in mode else sys.stdout
    if filename.endswith('.gz'):
        return gzip.open(filename, mode)
    else:
        return open(filename, mode)


###
# Read and filter input files
###

def read_segments(segments, max_ratio=1.2,
                  min_size=200000, min_n_baf=200, min_sd=0.4):

    if max_ratio >= 1:
        min_ratio = 1 / max_ratio
    else:
        min_ratio = max_ratio
        max_ratio = 1 / max_ratio

    header = next(segments)
    for line in segments:
        line = line.rstrip().replace('"', '').split('\t')
        try:
            ratio = float(line[6])
            n_baf = int(line[4])
            size = int(line[2]) - int(line[1])
            sd = float(line[8])
            if (ratio <= min_ratio or ratio >= max_ratio) and \
                n_baf >= min_n_baf and size >= min_size and \
                sd <= min_sd:
                yield '\t'.join([line[0], line[1], line[2], line[3], line[6],
                             line[9], line[10], line[11]])
        except ValueError:
            pass


def set_type_or_none(x, fn):
    try:
        return fn(x)
    except ValueError:
        return None


def bedpe_filter(bedpe, max_distance=10000):
    for line in bedpe:
        sv_id = line[6]
        baf = set_type_or_none(line[32], float)
        ratio = set_type_or_none(line[33], float)
        n_cnt = set_type_or_none(line[34], int)
        n_a = set_type_or_none(line[35], int)
        n_b = set_type_or_none(line[36], int)

        sv_chr1 = line[0]
        sv_n1 = int(line[1])
        sv_chr2 = line[3]
        sv_n2 = int(line[4])
        seg_chr = line[29]
        seg_n1 = int(line[30])
        seg_n2 = int(line[31])

        if (sv_chr1 == seg_chr):
            diff_1_1 = abs(sv_n1 - seg_n1)
            diff_1_2 = abs(sv_n1 - seg_n2)
        else:
            diff_1_1 = 2 * max_distance
            diff_1_2 = 2 * max_distance
        if (sv_chr2 == seg_chr):
            diff_2_1 = abs(sv_n2 - seg_n1)
            diff_2_2 = abs(sv_n2 - seg_n2)
        else:
            diff_2_1 = 2 * max_distance
            diff_2_2 = 2 * max_distance
        if diff_1_1 <= max_distance or diff_1_2 <= max_distance or \
                diff_2_1 <= max_distance or diff_2_2 <= max_distance:
            yield [sv_id, baf, ratio, n_cnt, n_a, n_b]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--vcf',  help='VCF input')
    parser.add_argument('-o', '--out',  help='VCF output', default='-')
    parser.add_argument('-s', '--segments',
                        help='Segment BED to annotate VCF', required=True)
    parser.add_argument('-b', '--bedpe',  help='Bedpe converted input vcf')
    args = parser.parse_args()
    segs_str = ''
    with xopen(args.segments, 'rt') as segments:
        segs_filter = read_segments(segments)
        for line in segs_filter:
            segs_str += '%s\n' % line
    segs_bed = pybedtools.BedTool(segs_str, from_string=True)
    bedpe = pybedtools.BedTool(args.bedpe)
    res_bedpe = bedpe.pair_to_bed(segs_bed, stream=True)
    filtered_bedpe = bedpe_filter(res_bedpe)

    overlap_dict = dict()
    for line in filtered_bedpe:
        sv_id, baf, ratio, n_cnt, n_a, n_b = line
        if sv_id not in overlap_dict.keys():
            overlap_dict[sv_id] = {
                'Bf': baf, 'ratio': ratio,
                'CNt': n_cnt, 'A': n_a, 'B': n_b}

    if args.out.endswith('.bcf'):
        write = 'wb'
    else:
        write = 'w'

    with VariantFile(args.vcf) as vcf_in:
        vcf_in.header.info.add('Bf', 1, 'Float',
                               'B allele frequency')
        vcf_in.header.info.add('depth.ratio', 1, 'Float',
                               'Depth ratio')
        vcf_in.header.info.add('CN_tot', 1, 'Integer',
                               'Estimated copy number of the tumor')
        vcf_in.header.info.add('CN_A', 1, 'Integer',
                               'Estimated copy number of A alleles')
        vcf_in.header.info.add('CN_B', 1, 'Integer',
                               'Estimated copy number of B alleles')
        vcf_in.header.info.add('CN_supp', 0, 'Flag',
                               'Copy number support')
        with VariantFile(args.out, write, header=vcf_in.header) as vcf_out:
            for rec in vcf_in.fetch():
                if rec.id in overlap_dict.keys():
                    if overlap_dict[rec.id]['Bf'] is not None:
                        rec.info['Bf'] = overlap_dict[rec.id]['Bf']
                    if overlap_dict[rec.id]['ratio'] is not None:
                        rec.info['depth.ratio'] = overlap_dict[rec.id]['ratio']
                    if overlap_dict[rec.id]['CNt'] is not None:
                        rec.info['CN_tot'] = overlap_dict[rec.id]['CNt']
                    if overlap_dict[rec.id]['A'] is not None:
                        rec.info['CN_A'] = overlap_dict[rec.id]['A']
                    if overlap_dict[rec.id]['B'] is not None:
                        rec.info['CN_B'] = overlap_dict[rec.id]['B']
                    rec.info['CN_supp'] = True
                vcf_out.write(rec)

if __name__ == "__main__":
    main()

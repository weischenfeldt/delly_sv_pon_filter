#!/bin/bash
RUN_ID=$1
RAW_VCF_FILE=$2
SEGMETATION_FILE=$3
BEDPE_FILE=$4
DIR=$5
if [[ $# -lt 5 ]]
then
echo -e "syntax: <run_id> <vcf_file> <segmetation_file> <bedpe_file> <output_directory>"
exit -1
fi
sed 's/\"//g' $SEGMETATION_FILE > $DIR/seg_tmp1.txt
sed '1d' $DIR/seg_tmp1.txt > $DIR/seg_tmp2.txt
rm $DIR/seg_tmp1.txt

cat $DIR/seg_tmp2.txt | awk '(($7<0.85) || ($7>1.2))  && $5>200 && $3-$2>200000 && $9 <0.4' | awk '{print $0,"CN"}'> $DIR/seg_tmp3.txt
bedtools pairtobed -a $BEDPE_FILE -b $DIR/seg_tmp3.txt > $DIR/$RUN_ID.overlap.bedpe.txt
rm -r $DIR/seg_tmp2.txt $DIR/seg_tmp3.txt
cat $DIR/$RUN_ID.overlap.bedpe.txt | awk '{print $0,($25-$2),($26-$3),($25-$5),($26-$6)}' > $DIR/$RUN_ID.overlap.diff.bedpe.txt
cat $DIR/$RUN_ID.overlap.diff.bedpe.txt | awk '{ print $0, ($38 >= 0) ? $38 : 0 - $38, ($39 >= 0) ? $39 : 0 - $39, ($40 >= 0) ? $40 : 0 - $40, ($41 >= 0) ? $41 : 0 - $41}' | awk '($42 <= 10000) || ($43 <= 10000) || ($44 <= 10000) || ($45 <= 10000)  {print ;}' > $DIR/$RUN_ID.overlap.filtered.bedpe.txt
awk -v OFS='\t' '{ print $1, $2, $3, $7, $27, $30, $33, $34, $35, $37 }' $DIR/$RUN_ID.overlap.filtered.bedpe.txt > $DIR/$RUN_ID.overlap.bed.txt
rm -r $DIR/$RUN_ID.overlap.bedpe.txt $DIR/$RUN_ID.overlap.diff.bedpe.txt  $DIR/$RUN_ID.overlap.filtered.bedpe.txt

echo -e "\n>> Annotate the Vcf <<\n"

time=`date +"%s"`

Annots_FILE=/tmp/$time\tmp.hdr

read -r -d '' hdr << EOM
##INFO=<ID=Bf,Number=1,Type=Float,Description="B allele frequency">
##INFO=<ID=depth.ratio,Number=1,Type=Float,Description="Depth ratio">
##INFO=<ID=CN_tot,Number=1,Type=Integer,Description="Estimated copy number of the tumor">
##INFO=<ID=CN_A,Number=1,Type=Integer,Description="Estimated number of A alleles">
##INFO=<ID=CN_B,Number=1,Type=Integer,Description="Estimated copy number of B alleles">
##INFO=<ID=CN_supp,Number=0,Type=String,Description="Copy number support">
EOM
echo -e "$hdr" > $Annots_FILE
echo $Annots_FILE
bgzip $DIR/$RUN_ID.overlap.bed.txt
tabix -s1 -b2 -e3 $DIR/$RUN_ID.overlap.bed.txt.gz
bcftools annotate -a $DIR/$RUN_ID.overlap.bed.txt.gz -h $Annots_FILE -c CHROM,POS,-,ID,Bf,depth.ratio,CN_tot,CN_A,CN_B,CN_supp $RAW_VCF_FILE > $DIR/$RUN_ID.annotated.raw.vcf
bgzip $DIR/$RUN_ID.annotated.raw.vcf
echo -e $DIR/$RUN_ID.annotated.raw.vcf.gz
